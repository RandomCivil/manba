package main

import (
	"io/ioutil"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/manba/config"
	"gitlab.com/RandomCivil/manba/core"
	"gopkg.in/yaml.v2"
)

var (
	serverConfig = &config.ServerConfig{}
	serverCmd    = &cobra.Command{Use: "server"}
)

func init() {
	serverCmd.Flags().IntVarP(&serverConfig.Listen, "listen", "l", 8443, "server listening port")
	serverCmd.Flags().IntVarP(&serverConfig.PreferIpv6, "prefer-ipv6", "i", 1, "ipv6 first,0:disable,1:enable")
	serverCmd.Flags().IntVarP(&serverConfig.DnsCleanInterval, "dns-clean-interval", "", DNS_CACHE_EXPIRE, "dns clean interval")
	serverCmd.Flags().StringVarP(&serverConfig.CertPath, "cert", "c", "", "cert path")
	serverCmd.Flags().StringVarP(&serverConfig.KeyPath, "key", "k", "", "key path")
	serverCmd.Flags().StringVarP(&serverConfig.Url, "url", "", "/test", "put method url")
	serverCmd.Flags().StringVarP(&serverConfig.AuthFile, "auth", "a", "", "auth file path")
	serverCmd.Flags().StringVarP(&serverConfig.Forward, "forward", "f", "", "forward url")

	serverCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, serverConfig)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("config from file:%+v", serverConfig)

		certPEMBlock, err := ioutil.ReadFile(serverConfig.CertPath)
		if err != nil {
			panic(err)
		}
		keyPEMBlock, err := ioutil.ReadFile(serverConfig.KeyPath)
		if err != nil {
			panic(err)
		}

		var yamlBytes []byte
		if serverConfig.AuthFile != "" {
			var err error
			yamlBytes, err = ioutil.ReadFile(serverConfig.AuthFile)
			if err != nil {
				panic(err)
			}
		}

		s := core.NewHttp2Server(serverConfig)
		s.Serve(certPEMBlock, keyPEMBlock, yamlBytes)
	}
}
