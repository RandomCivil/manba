package main

import (
	"flag"
	"io/ioutil"
	"os"
	"runtime/trace"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/common"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/common/profile"
)

const (
	DNS_CACHE_EXPIRE = 24 * 60 * 60
)

var (
	openTrace int
	traceFile *os.File
	confPath  string
	confBytes []byte
	rootCmd   *cobra.Command
	docCmd    = &cobra.Command{Use: "doc"}

	homePath = os.Getenv("HOME")
	version  = flag.Bool("version", false, "Show current version")
)

func init() {
	rootCmd = &cobra.Command{
		Use: "manba",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			log.New()
			if openTrace == 1 {
				log.DefaultLogger.Infof("start trace")
				traceFile = profile.DoTrace()
			}

			if confPath != "" {
				var err error
				confBytes, err = ioutil.ReadFile(confPath)
				if err != nil {
					log.DefaultLogger.Errorf("failed to read config file %s,err:%v", confPath, err)
				}
			}
		},
		PersistentPostRun: func(cmd *cobra.Command, args []string) {
			if openTrace == 1 {
				log.DefaultLogger.Infof("stop trace")
				trace.Stop()
				if err := traceFile.Close(); err != nil {
					log.DefaultLogger.Errorf("failed to close trace file: %v", err)
					return
				}
			}
		},
	}
	rootCmd.AddCommand(serverCmd)
	rootCmd.AddCommand(clientCmd)
	rootCmd.AddCommand(docCmd)

	rootCmd.PersistentFlags().IntVarP(&openTrace, "trace", "t", 0, "open trace,1:enable,0:disable")
	rootCmd.PersistentFlags().IntVarP(&log.Level, "log", "", 3, "log level.1:error,2:info,3:debug")
	rootCmd.PersistentFlags().StringVarP(&confPath, "conf", "", "", "config file")
}

func main() {
	flag.Parse()
	common.Verbose()
	if *version {
		return
	}

	if err := rootCmd.Execute(); err != nil {
		log.DefaultLogger.Errorf("cmd execute err:%v", err)
		os.Exit(1)
	}
}
