package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/RandomCivil/manba/config"
	"gitlab.com/RandomCivil/manba/core"
	"gopkg.in/yaml.v2"
)

var (
	clientConfig = &config.ClientConfig{}
	clientCmd    = &cobra.Command{Use: "client"}
)

func init() {
	clientCmd.Flags().IntVarP(&clientConfig.Listen, "listen", "l", 4431, "client listening port")
	clientCmd.Flags().StringVarP(&clientConfig.User, "user", "u", "admin", "user name")
	clientCmd.Flags().StringVarP(&clientConfig.Pwd, "pwd", "p", "123456", "password")
	clientCmd.Flags().StringVarP(&clientConfig.ServerAddr, "server", "s", "127.0.0.1:8443", "server address")
	clientCmd.Flags().StringVarP(&clientConfig.Url, "url", "", "/test", "server url")
	clientCmd.Flags().StringVarP(&clientConfig.Method, "method", "m", "put", "client method:connect,put")
	clientCmd.Flags().IntVarP(&clientConfig.ConnNum, "connection-num", "n", 5, "num of connections")
	clientCmd.Flags().StringVarP(&clientConfig.GfwListPath, "gfwlist-path", "", homePath+"/gfwlist.txt", "gfwlist path")
	clientCmd.Flags().StringVarP(&clientConfig.GeoIP2Path, "geoip2-path", "", homePath+"/GeoLite2-Country.mmdb", "geoip2 data path")
	clientCmd.Flags().IntVarP(&clientConfig.RouteStrategy, "route-strategy", "r", 2, "client route stategy")
	clientCmd.Flags().StringVarP(&clientConfig.ServerName, "server-name", "", "www.microsoft.com", "client hello server name")
	clientCmd.Flags().IntVarP(&clientConfig.DnsCleanInterval, "dns-clean-interval", "", DNS_CACHE_EXPIRE, "dns clean interval")

	clientCmd.Run = func(cmd *cobra.Command, args []string) {
		err := yaml.Unmarshal(confBytes, clientConfig)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("config from file:%+v", clientConfig)

		c := core.NewHttp2Client(clientConfig)
		c.Serve()
	}
}
