package config

type ServerConfig struct {
	Listen           int
	PreferIpv6       int    `yaml:"prefer-ipv6"`
	CertPath         string `yaml:"cert"`
	KeyPath          string `yaml:"key"`
	Url              string
	AuthFile         string `yaml:"auth"`
	DnsCleanInterval int    `yaml:"dns-clean-interval"`
	Forward          string
	Domain           string
}

type ClientConfig struct {
	ServerAddr       string `yaml:"server"`
	Listen           int
	User             string
	Pwd              string
	Url              string
	Method           string
	ConnNum          int `yaml:"connection-num"`
	RouteConfig      `yaml:",inline"`
	ServerName       string `yaml:"server-name"`
	DnsCleanInterval int    `yaml:"dns-clean-interval"`
}

type RouteConfig struct {
	GfwListPath   string `yaml:"gfwlist-path"`
	GeoIP2Path    string `yaml:"geoip2-path"`
	RouteStrategy int    `yaml:"route-strategy"`
}

func (c *RouteConfig) GetGfwListPath() string {
	return c.GfwListPath
}

func (c *RouteConfig) GetGeoIP2Path() string {
	return c.GeoIP2Path
}

func (c *RouteConfig) GetRouteStrategy() int {
	return c.RouteStrategy
}
