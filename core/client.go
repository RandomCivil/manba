package core

import (
	"bufio"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"io"
	"net"
	"net/http"
	"net/url"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/manba/config"
	"gitlab.com/RandomCivil/manba/network"
	"golang.org/x/net/http2"
)

type http2Client struct {
	signal   *sn.Signal
	config   *config.ClientConfig
	done     chan struct{}
	ln       net.Listener
	mu       sync.Mutex
	ch       chan *h2Client
	router   *network.Router
	dnsCache *network.DnsCache
}

func NewHttp2Client(conf *config.ClientConfig) *http2Client {
	dnsCache := network.NewDnsCache(conf.DnsCleanInterval)
	dnsCache.SetPriority(false)
	go dnsCache.CleanTimer(conf.DnsCleanInterval)

	return &http2Client{
		signal:   sn.NewSignal(),
		config:   conf,
		done:     make(chan struct{}, 1),
		mu:       sync.Mutex{},
		ch:       make(chan *h2Client, conf.ConnNum),
		router:   network.NewRouter(conf),
		dnsCache: dnsCache,
	}
}

var cid = new(clientId)

type clientId struct {
	i int32
}

func (ci *clientId) index() int32 {
	return atomic.AddInt32(&ci.i, 1)
}

type h2Client struct {
	c *http.Client
	n int32
	i int32
}

func (c *h2Client) add(delta int32) {
	v := atomic.AddInt32(&c.n, delta)
	if delta == 1 {
		log.DefaultLogger.Infof("fetch client %d which stream nums is %d", c.i, v-1)
	} else {
		log.DefaultLogger.Infof("client %d which stream num is %d is about to close", c.i, v+1)
	}
}

func leastStreamFirst(ch chan *h2Client) *h2Client {
	var (
		min     int32
		r       *h2Client
		isFirst = true
		i       int32
	)
	for {
		s := <-ch
		if isFirst {
			i = s.i
			r = s
			min = s.n
			isFirst = false
			ch <- s
			continue
		}
		log.DefaultLogger.Debugf("cur:%d min:%d cur_i:%d i:%d len:%d", s.n, min, s.i, i, len(ch))
		if !isFirst && s.i == i {
			break
		}
		if s.n < min {
			ch <- r
			min = s.n
			r = s
		} else {
			ch <- s
		}
	}
	log.DefaultLogger.Debugf("choose client %d,num:%d", r.i, r.n)
	return r
}

func (c *http2Client) fetchClient() *h2Client {
	l := len(c.ch)
	if l >= c.config.ConnNum {
		return leastStreamFirst(c.ch)
	}
	serverDomain, _, _ := net.SplitHostPort(c.config.ServerAddr)
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		NextProtos:         []string{http2.NextProtoTLS},
		ServerName:         c.config.ServerName,
	}
	log.DefaultLogger.Infof("server domain:%s", serverDomain)

	h1Tr := &http.Transport{
		// MaxIdleConns controls the maximum number of idle (keep-alive)
		// connections across all hosts.
		//MaxIdleConns:        100,
		//MaxIdleConnsPerHost: 5,
		// MaxConnsPerHost optionally limits the total number of
		// connections per host, including connections in the dialing,
		// active, and idle states. On limit violation, dials will block.
		//MaxConnsPerHost: 20,
		// IdleConnTimeout is the maximum amount of time an idle
		// (keep-alive) connection will remain idle before closing
		// itself.
		IdleConnTimeout: 100 * time.Second,
	}
	h2Tr := &http2.Transport{
		TLSClientConfig: tlsConfig,
	}
	http2.ConfigureTransport(h1Tr)
	client := &http.Client{
		// Timeout specifies a time limit for requests made by this
		// Client. The timeout includes connection time, any
		// redirects, and reading the response body. The timer remains
		// running after Get, Head, Post, or Do return and will
		// interrupt reading of the Response.Body.
		Timeout:   time.Second * 180,
		Transport: h2Tr,
	}
	h2c := &h2Client{c: client, i: cid.index()}
	return h2c
}

func (c *http2Client) handleTunneling(w http.ResponseWriter, r *http.Request) {
	conn := network.ConnFromHijack(w)
	if conn == nil {
		return
	}
	defer conn.Close()

	host := r.Host
	rawHost, port, err := net.SplitHostPort(host)
	if port == "" {
		host = net.JoinHostPort(host, "80")
	}
	log.DefaultLogger.Debugf("rawHost:%s.port:%s,err:%v", rawHost, port, err)

	routeStrategy := c.config.GetRouteStrategy()
	if routeStrategy > 0 && err == nil {
		direct, ip, err := c.router.Route(rawHost, c.dnsCache, routeStrategy)
		if err != nil {
			log.DefaultLogger.Infof("route err:%v,host:%s", err, host)
			return
		}
		log.DefaultLogger.Infof("host:%s,direct:%v,ip:%v", rawHost, direct, ip)

		if direct {
			c.directReq(host, conn)
			return
		}
		if routeStrategy == 2 && ip != nil {
			host = net.JoinHostPort(ip.String(), "443")
		}
	}

	pr, pw := io.Pipe()
	defer pr.Close()
	defer pw.Close()

	req := &http.Request{
		Header: make(http.Header),
		Host:   host,
		URL: &url.URL{
			Scheme: "https",
			Host:   c.config.ServerAddr,
			Path:   c.config.Url,
		},
		Proto:         "HTTP/2.0",
		ProtoMajor:    2,
		ProtoMinor:    0,
		Body:          pr,
		ContentLength: -1,
	}
	if c.config.User != "" && c.config.Pwd != "" {
		req.Header.Set("Proxy-Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(c.config.User+":"+c.config.Pwd)))
	}

	if r.Method == http.MethodConnect {
		req.Method = strings.ToUpper(c.config.Method)
	} else {
		req.Method = strings.ToUpper(r.Method)
	}

	c.mu.Lock()
	h2c := c.fetchClient()
	c.mu.Unlock()
	c.ch <- h2c

	h2c.add(1)
	NewPiper(conn, pw, h2c, r, req, c.config).Pipe()
	h2c.add(-1)
}

func writePw(r *http.Request, errCh chan error, pw *io.PipeWriter) {
	if err := r.Write(pw); err != nil {
		errCh <- err
	}
}

func (c *http2Client) directReq(host string, conn net.Conn) {
	target, err := network.ConnectTarget(c.dnsCache, host)
	if err != nil {
		log.DefaultLogger.Errorf("direct connect %s err %v", host, err)
		return
	}
	defer target.Close()
	conn.Write([]byte("HTTP/1.1 200 Connection established\r\n\r\n"))

	errCh := make(chan error, 2)
	go network.Proxy(target, conn, errCh)
	go network.Proxy(conn, target, errCh)
	<-errCh
}

func (c *http2Client) Serve() {
	runtime.GC()
	server := &http.Server{
		Addr: ":" + strconv.Itoa(c.config.Listen),
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.DefaultLogger.Infof("Received request %s %s %s %s\n",
				r.Method,
				r.Host,
				r.RemoteAddr,
				r.RequestURI,
			)
			c.handleTunneling(w, r)
		}),
	}

	l, err := net.Listen("tcp", ":"+strconv.Itoa(c.config.Listen))
	if err != nil {
		panic(err)
	}

	c.ln = l
	c.done <- struct{}{}
	log.DefaultLogger.Infof("client started at " + l.Addr().String())

	errCh := make(chan error)

	go c.signal.Handle(server)
	go func() {
		err := server.Serve(l)
		log.DefaultLogger.Errorf("err:%v", err)
		errCh <- err
	}()
	select {
	case <-errCh:
	}
}

type Piper interface {
	Pipe()
}

func NewPiper(conn net.Conn, pw *io.PipeWriter, h2c *h2Client, r, req *http.Request, config *config.ClientConfig) Piper {
	if r.Method == http.MethodConnect {
		return &connectClientHandler{
			config: config,
			commonClientHandler: &commonClientHandler{
				conn: conn,
				pw:   pw,
				h2c:  h2c,
				req:  req,
			},
		}
	} else {
		return &nonConnectClientHandler{
			r: r,
			commonClientHandler: &commonClientHandler{
				conn: conn,
				pw:   pw,
				h2c:  h2c,
				req:  req,
			},
		}
	}
}

type commonClientHandler struct {
	conn net.Conn
	pw   *io.PipeWriter
	h2c  *h2Client
	req  *http.Request
}

func (h *commonClientHandler) Do() (*http.Response, error) {
	resp, err := h.h2c.c.Do(h.req)
	if err != nil {
		log.DefaultLogger.Errorf("error connect proxy server request: %v", err)
		return nil, err
	}

	if resp.StatusCode != 200 {
		log.DefaultLogger.Errorf("proxy server resp status:%d,proto:%s", resp.StatusCode, resp.Proto)
		return nil, errors.New("invalid response http code")
	} else {
		log.DefaultLogger.Debugf("proxy server resp status:%d,proto:%s", resp.StatusCode, resp.Proto)
	}
	return resp, nil
}

type connectClientHandler struct {
	config *config.ClientConfig
	*commonClientHandler
}

func (h *connectClientHandler) Pipe() {
	h.req.Method = strings.ToUpper(h.config.Method)
	resp, err := h.Do()
	if err != nil {
		return
	}
	defer resp.Body.Close()

	errCh := make(chan error, 2)
	go network.Proxy(h.pw, h.conn, errCh)
	go network.Proxy(h.conn, resp.Body, errCh)

	err = <-errCh
	log.DefaultLogger.Infof("client %d pipe err:%v", h.h2c.i, err)
}

type nonConnectClientHandler struct {
	r *http.Request
	*commonClientHandler
}

func (h *nonConnectClientHandler) Pipe() {
	go func() {
		if err := h.r.Write(h.pw); err != nil {
			log.DefaultLogger.Errorf("write to pw err: %v", err)
		}
		h.pw.Close()
	}()

	resp, err := h.Do()
	if err != nil {
		return
	}
	defer resp.Body.Close()

	responseReader := bufio.NewReaderSize(resp.Body, 10240)
	rr, err := http.ReadResponse(responseReader, h.r)
	if err != nil {
		log.DefaultLogger.Errorf("http.ReadResponse err %v", err)
		return
	}

	log.DefaultLogger.Infof("resp len:%v,rr len:%v", resp.ContentLength, rr.ContentLength)

	if err := rr.Write(h.conn); err != nil {
		log.DefaultLogger.Errorf("rr.Write err %v", err)
	}
}
