package core

import (
	"crypto/tls"
	"io"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/RandomCivil/common/log"
	sn "gitlab.com/RandomCivil/common/signal"
	"gitlab.com/RandomCivil/manba/auth"
	"gitlab.com/RandomCivil/manba/config"
	"gitlab.com/RandomCivil/manba/network"
	"golang.org/x/net/http2"
)

const (
	MAX_READ_FRAME_SIZE = 32 * 1024
	FORWARDED_KEY       = "X-Forwarded-For"
)

var (
	matchResTypeRe = regexp.MustCompile(`\.\w+$`)
	contentTypeMap = map[string]string{
		".js":   "application/javascript; charset=utf-8",
		".css":  "text/css",
		".png":  "image/png",
		".html": "text/html; charset=utf-8",
	}
	h2Server = http2.Server{
		// MaxReadFrameSize optionally specifies the largest frame this server is willing to read. A valid value is between 16k and 16M, inclusive
		MaxReadFrameSize: MAX_READ_FRAME_SIZE,
		// IdleTimeout specifies how long until idle clients should be
		// closed with a GOAWAY frame. PING frames are not considered
		// activity for the purposes of IdleTimeout.
		IdleTimeout: 60 * time.Second,
	}
)

type http2Server struct {
	signal   *sn.Signal
	dnsCache *network.DnsCache
	config   *config.ServerConfig
	Done     chan struct{}
	ln       net.Listener
	tc       *timeoutConfig
	authMap  map[string]string
}

type timeoutConfig struct {
	idleTimeout, readTimeout, writeTimeout int
}

func NewHttp2Server(conf *config.ServerConfig) *http2Server {
	dnsCache := network.NewDnsCache(conf.DnsCleanInterval)
	dnsCache.SetPriority(conf.PreferIpv6 == 1)
	go dnsCache.CleanTimer(conf.DnsCleanInterval)

	tc := &timeoutConfig{
		idleTimeout:  30,
		readTimeout:  30,
		writeTimeout: 30,
	}
	server := &http2Server{
		signal:   sn.NewSignal(),
		dnsCache: dnsCache,
		config:   conf,
		Done:     make(chan struct{}, 1),
		tc:       tc,
		authMap:  make(map[string]string),
	}
	return server
}

type flushWriter struct {
	w io.Writer
}

func (fw flushWriter) Write(p []byte) (n int, err error) {
	n, err = fw.w.Write(p)
	if err != nil {
		log.DefaultLogger.Errorf("flush writer err:%v", err)
		return
	}
	if f, ok := fw.w.(http.Flusher); ok {
		f.Flush()
	}
	return
}

func (s *http2Server) checkAuth(r *http.Request) bool {
	if s.authMap != nil && !auth.ParseBasicAuth(s.authMap, r.Header.Get("Proxy-Authorization")) {
		return false
	}
	return true
}

func (s *http2Server) handleProxy(w http.ResponseWriter, r *http.Request) {
	if !s.checkAuth(r) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	target, err := network.ConnectTarget(s.dnsCache, r.Host)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	target.SetDeadline(time.Now().Add(30 * time.Second))
	defer target.Close()
	defer r.Body.Close()

	NewServerDuplexCopier(r, w, target).DuplexCopy()
}

func (s *http2Server) handleHTTP(w http.ResponseWriter, r *http.Request) {
	client := http.Client{
		Timeout: time.Second * 60,
	}
	url := s.config.Forward + r.RequestURI
	forwardReq, _ := http.NewRequest("GET", url, nil)
	log.DefaultLogger.Infof("req header:%+v", r.Header)
	if v, ok := r.Header[FORWARDED_KEY]; ok {
		forwardReq.Header.Add(FORWARDED_KEY, strings.Join(v, ","))
	}
	log.DefaultLogger.Infof("forward req header:%+v", forwardReq.Header)

	resp, err := client.Do(forwardReq)
	if err != nil {
		log.DefaultLogger.Errorf("url %s error making request: %v", url, err)
		return
	}
	defer resp.Body.Close()
	log.DefaultLogger.Infof("forward url:%s,status:%d,proto:%s", url, resp.StatusCode, resp.Proto)
	log.DefaultLogger.Infof("forward target response header:%+v", resp.Header)

	suffix := matchResTypeRe.FindString(r.RequestURI)
	log.DefaultLogger.Debugf("url suffix:%s", suffix)

	for k, v := range resp.Header {
		s := strings.Join(v, ",")
		w.Header().Set(k, s)
	}
	w.WriteHeader(resp.StatusCode)

	io.Copy(w, resp.Body)
	if fw, ok := w.(http.Flusher); ok {
		fw.Flush()
	}
}

func (s *http2Server) Serve(certPEMBlock, keyPEMBlock, yamlBytes []byte) error {
	var err error
	s.authMap, err = auth.LoadAuthList(yamlBytes)
	if err != nil {
		log.DefaultLogger.Errorf("err:%v", err)
		s.Done <- struct{}{}
		return err
	}

	cert, err := tls.X509KeyPair(certPEMBlock, keyPEMBlock)
	if err != nil {
		log.DefaultLogger.Errorf("err:%v", err)
		s.Done <- struct{}{}
		return err
	}

	cfg := &tls.Config{
		Certificates:             []tls.Certificate{cert},
		NextProtos:               []string{http2.NextProtoTLS},
		MinVersion:               tls.VersionTLS13,
		CurvePreferences:         []tls.CurveID{tls.X25519},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_AES_128_GCM_SHA256,
			tls.TLS_CHACHA20_POLY1305_SHA256,
		},
	}

	l, err := tls.Listen("tcp", ":"+strconv.Itoa(s.config.Listen), cfg)
	if err != nil {
		panic(err)
	}

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.DefaultLogger.Infof("Received request %s %s %s %s\n",
			r.Method,
			r.Host,
			r.RemoteAddr,
			r.RequestURI,
		)
		hostName, _ := network.GetHostNamePort(r.Host, r.TLS != nil)
		if hostName == s.config.Domain {
			s.handleHTTP(w, r)
			return
		}
		if r.Method == http.MethodPut && r.RequestURI != s.config.Url {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		s.handleProxy(w, r)
	})
	server := &http.Server{
		// IdleTimeout is the maximum amount of time to wait for the
		// next request when keep-alives are enabled
		IdleTimeout:  time.Duration(s.tc.idleTimeout) * time.Second,
		ReadTimeout:  time.Duration(s.tc.readTimeout) * time.Second,
		WriteTimeout: time.Duration(s.tc.writeTimeout) * time.Second,
		Handler:      handler,
	}
	http2.ConfigureServer(server, &h2Server)

	s.ln = l
	s.Done <- struct{}{}
	log.DefaultLogger.Infof("server started at " + l.Addr().String())

	errCh := make(chan error)
	go s.signal.Handle(l)
	go func() {
		errCh <- server.Serve(l)
	}()
	<-errCh
	return nil
}

type ServerDuplexCopier interface {
	DuplexCopy()
}

func NewServerDuplexCopier(r *http.Request, w http.ResponseWriter, remoteTarget net.Conn) ServerDuplexCopier {
	if r.Method == http.MethodConnect || r.Method == http.MethodPut {
		return &connectServerHandler{
			w:            w,
			r:            r,
			remoteTarget: remoteTarget,
		}
	}
	return &nonConnectServerHandler{
		w:            w,
		r:            r,
		remoteTarget: remoteTarget,
	}
}

type connectServerHandler struct {
	w            http.ResponseWriter
	r            *http.Request
	remoteTarget net.Conn
}

func (h *connectServerHandler) DuplexCopy() {
	h.w.Write([]byte("HTTP/1.1 200 Connection established\r\n\r\n"))
	if fw, ok := h.w.(http.Flusher); ok {
		fw.Flush()
	}

	errCh := make(chan error, 2)
	go network.Proxy(h.remoteTarget, h.r.Body, errCh)
	go network.Proxy(flushWriter{w: h.w}, h.remoteTarget, errCh)
	e := <-errCh
	log.DefaultLogger.Infof("handleTunneling err:%v", e)
}

type nonConnectServerHandler struct {
	w            http.ResponseWriter
	r            *http.Request
	remoteTarget net.Conn
}

func (h *nonConnectServerHandler) DuplexCopy() {
	n, err := network.Copy(h.remoteTarget, h.r.Body)
	log.DefaultLogger.Infof("nonConnectHandler client -> server %v %v", n, err)

	n, err = network.Copy(flushWriter{w: h.w}, h.remoteTarget)
	log.DefaultLogger.Infof("nonConnectHandler server -> client %v %v", n, err)
}
