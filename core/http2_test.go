package core

import (
	"crypto/tls"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"testing"
	"time"

	"gitlab.com/RandomCivil/common/cert"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/manba/config"
	"gitlab.com/RandomCivil/manba/network"
	"golang.org/x/net/http2"
)

var (
	HTTPS_HOST = "https://localhost:8445/"
	HTTP_HOST  = "http://localhost:8083/"
	KEY, CERT  []byte
	AUTH       = `
- user: admin
  pwd: 123456
`
	testRootPath string
)

func init() {
	log.Level = 2
	log.New()
	CERT, KEY = cert.GenCert()

	ciEnv := os.Getenv("CI")
	if ciEnv == "true" {
		testRootPath = "/go/src/manba"
	} else {
		testRootPath = os.Getenv("HOME")
	}
}

func TestMain(m *testing.M) {
	ms := &mockServer{
		stopCh: make(chan struct{}, 1),
		doneCh: make(chan struct{}, 1),
	}
	go ms.start(8445, 8083)
	<-ms.doneCh

	r := m.Run()
	os.Exit(r)
	ms.stop()
}

type mockServer struct {
	stopCh, doneCh chan struct{}
	ln, h2ln       net.Listener
}

func (m *mockServer) stop() {
	m.stopCh <- struct{}{}
}

func (m *mockServer) handleConn(conn net.Conn) {
	defer conn.Close()
	if tlsConn, ok := conn.(*tls.Conn); ok {
		e := tlsConn.Handshake()
		log.DefaultLogger.Infof("handshake err:%v", e)
		state := tlsConn.ConnectionState()
		log.DefaultLogger.Infof("HandshakeComplete:%v,CipherSuite:%v", state.HandshakeComplete, state.CipherSuite)
	}

	//conn.Write([]byte("HTTP/1.1 200 hello\r\n\r\n"))
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("hello"))
	})
	h2Server := http2.Server{}
	h2Server.ServeConn(conn, &http2.ServeConnOpts{
		BaseConfig: &http.Server{},
		Handler:    handler,
	})
}

func (m *mockServer) tlsListenLoop() {
	for {
		conn, err := m.h2ln.Accept()
		if err != nil {
			log.DefaultLogger.Errorf("accept err:%v", err)
			return
		}
		go m.handleConn(conn)
		select {
		case <-m.stopCh:
			return
		default:
		}
	}
}

func (m *mockServer) start(httpsPort, httpPort int) {
	mockCert, mockKey := cert.GenCert()
	cert, err := tls.X509KeyPair(mockCert, mockKey)
	if err != nil {
		return
	}
	tlsConfig := &tls.Config{
		Certificates:             []tls.Certificate{cert},
		NextProtos:               []string{http2.NextProtoTLS},
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.X25519},
		PreferServerCipherSuites: true,
	}

	h2ln, err := tls.Listen("tcp", ":"+strconv.Itoa(httpsPort), tlsConfig)
	if err != nil {
		panic(err)
	}
	m.h2ln = h2ln

	ln, err := net.Listen("tcp", ":"+strconv.Itoa(httpPort))
	if err != nil {
		panic(err)
	}
	m.ln = ln

	go func() {
		log.DefaultLogger.Infof("mock http2 server start")
		m.tlsListenLoop()
	}()

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte("hello http2"))
	})

	errCh := make(chan error)
	go func() {
		log.DefaultLogger.Infof("mock http server start")
		err := http.Serve(ln, handler)
		log.DefaultLogger.Infof("err:%v", err)
		errCh <- err
	}()

	m.doneCh <- struct{}{}
	select {
	case <-m.stopCh:
		return
	case <-errCh:
		return
	}
}

func newServerDefaultConf() *config.ServerConfig {
	c := new(config.ServerConfig)
	c.Listen = 0
	c.PreferIpv6 = 0
	c.Url = "/test"
	c.DnsCleanInterval = 60
	c.Forward = "https://theviper.world"
	return c
}

func newClientDefaultConf() *config.ClientConfig {
	c := new(config.ClientConfig)
	c.Listen = 0
	c.Url = "/test"
	c.Method = "put"
	c.ConnNum = 1
	c.RouteStrategy = 0
	c.DnsCleanInterval = 60
	c.GeoIP2Path = testRootPath + "/GeoLite2-Country.mmdb"
	c.GfwListPath = testRootPath + "/gfwlist.txt"
	return c
}

func TestPipe(t *testing.T) {
	ms := &mockServer{
		stopCh: make(chan struct{}, 1),
		doneCh: make(chan struct{}, 1),
	}
	go ms.start(8446, 8085)
	<-ms.doneCh

	tc := &timeoutConfig{
		idleTimeout:  1,
		readTimeout:  1,
		writeTimeout: 1,
	}
	server := startServer(newServerDefaultConf(), tc, "", t)
	defer server.ln.Close()

	clientConfig := newClientDefaultConf()
	clientConfig.ServerAddr = server.ln.Addr().String()
	client := startClient(clientConfig, t)
	defer client.ln.Close()

	//resp, err := makeRequest("https://localhost:8446/", client.ln.Addr().String(), t)
	resp, err := makeRequest("https://127.0.0.1:8446/", client.ln.Addr().String(), t)
	if err != nil {
		t.Errorf("TestNoAuth error,err:%v,resp:%v,url:%s", err, resp, HTTPS_HOST)
	}
	t.Log(err)

	ms.stop()
	time.Sleep(1500 * time.Millisecond)
}

func TestUrl(t *testing.T) {
	tests := []struct {
		method, url string
		errEqNil    bool
	}{
		{"put", "/test", false},
		{"put", "/test1", true},
		{"connect", "/test", false},
		{"connect", "/test1", false},
	}
	server := startServer(newServerDefaultConf(), nil, "", t)
	defer server.ln.Close()

	for _, tt := range tests {
		clientConfig := newClientDefaultConf()
		clientConfig.ServerAddr = server.ln.Addr().String()
		clientConfig.Method = tt.method
		clientConfig.Url = tt.url
		client := startClient(clientConfig, t)
		resp, err := makeRequest(HTTPS_HOST, client.ln.Addr().String(), t)
		if tt.errEqNil {
			if err == nil {
				t.Fatalf("TestUrl error,err:%v,resp:%v", err, resp)
			}
		} else {
			if err != nil {
				t.Fatalf("TestUrl error,err:%v,resp:%v", err, resp)
			}
			t.Log(resp.StatusCode, resp.Proto)
			resp.Body.Close()
		}
		client.ln.Close()
	}
}

func TestNoAuth(t *testing.T) {
	tests := []struct {
		reqUrl, method string
	}{
		{HTTPS_HOST, "put"},
		{HTTPS_HOST, "connect"},
		{HTTP_HOST, "put"},
		{HTTP_HOST, "connect"},
	}
	server := startServer(newServerDefaultConf(), nil, "", t)
	defer server.ln.Close()

	for _, tt := range tests {
		clientConfig := newClientDefaultConf()
		clientConfig.ServerAddr = server.ln.Addr().String()
		clientConfig.Method = tt.method
		client := startClient(clientConfig, t)
		for i := 0; i < 2; i++ {
			resp, err := makeRequest(tt.reqUrl, client.ln.Addr().String(), t)
			if err != nil {
				t.Errorf("TestNoAuth error,err:%v,resp:%v,url:%s", err, resp, tt.reqUrl)
			}
			t.Log(resp.StatusCode, resp.Proto)
			resp.Body.Close()
		}
		client.ln.Close()
	}
}

func TestRouteStrategy(t *testing.T) {
	tests := []struct {
		reqUrl   string
		strategy int
	}{
		{"http://www.baidu.com", 1},
		{"https://www.baidu.com", 1},
		{"https://www.shopify.com", 1},
		{"https://www.github.com", 1},
		{"https://www.shopify.com", 2},
		{"https://www.github.com", 2},
	}
	sc := newServerDefaultConf()
	cc := newClientDefaultConf()

	for _, tt := range tests {
		cc.RouteStrategy = tt.strategy
		server, client := makeBothStart(sc, cc, "", t)
		resp, err := makeRequest(tt.reqUrl, client.ln.Addr().String(), t)
		if err != nil {
			t.Fatal("TestRouteStrategy fail", err)
		}
		t.Log(resp.StatusCode, err)
		t.Log(resp.Proto)
		resp.Body.Close()
		client.ln.Close()
		server.ln.Close()
		time.Sleep(100 * time.Millisecond)
	}
}

func TestRouteWrongDomain(t *testing.T) {
	sc := newServerDefaultConf()

	tests := []struct {
		reqUrl   string
		strategy int
	}{
		{"https://fjeowfjogjweogw.com", 1},
		{"https://fjeowfjogjweogw.com", 2},
	}
	for _, tt := range tests {
		cc := newClientDefaultConf()
		cc.RouteStrategy = tt.strategy
		server, client := makeBothStart(sc, cc, "", t)
		_, err := makeRequest(tt.reqUrl, client.ln.Addr().String(), t)
		t.Log(err)
		if err == nil {
			t.Fatal("TestRouteWrongDomain fail", err)
		}
		client.ln.Close()
		server.ln.Close()
	}
}

func TestAuth(t *testing.T) {
	tests := []struct {
		user, pwd string
		errEqNil  bool
	}{
		{"admin", "123456", false},
		{"admin1", "123456", true},
		{"admin", "12345", true},
		{"", "", true},
	}
	server := startServer(newServerDefaultConf(), nil, AUTH, t)
	defer server.ln.Close()

	for _, tt := range tests {
		clientConfig := newClientDefaultConf()
		clientConfig.ServerAddr = server.ln.Addr().String()
		clientConfig.User = tt.user
		clientConfig.Pwd = tt.pwd

		client := startClient(clientConfig, t)
		resp, err := makeRequest(HTTPS_HOST, client.ln.Addr().String(), t)
		if tt.errEqNil {
			if err == nil {
				t.Fatalf("TestAuth error,err:%v,resp:%v", err, resp)
			}
		} else {
			if err != nil {
				t.Fatalf("TestAuth error,err:%v,resp:%v", err, resp)
			}
			t.Log(resp.StatusCode, resp.Proto)
			resp.Body.Close()
		}
		client.ln.Close()
		time.Sleep(500 * time.Millisecond)
	}
}

func TestIPV6(t *testing.T) {
	tests := []struct{ reqUrl string }{
		{"https://localhost:8445/"},
		{"http://localhost:8083/"},
	}
	serverConfig := newServerDefaultConf()
	serverConfig.PreferIpv6 = 1
	server := startServer(serverConfig, nil, "", t)
	defer server.ln.Close()

	for _, tt := range tests {
		clientConfig := newClientDefaultConf()
		clientConfig.ServerAddr = server.ln.Addr().String()
		client := startClient(clientConfig, t)
		resp, err := makeRequest(tt.reqUrl, client.ln.Addr().String(), t)
		if err != nil {
			t.Errorf("TestNoAuth error,err:%v,resp:%v,url:%s", err, resp, tt.reqUrl)
		}
		t.Log(resp.StatusCode, resp.Proto)
		resp.Body.Close()
		client.ln.Close()
		time.Sleep(500 * time.Millisecond)
	}
}

func TestWrongDns(t *testing.T) {
	server, client := makeBothStart(newServerDefaultConf(), newClientDefaultConf(), "", t)
	defer server.ln.Close()
	defer client.ln.Close()

	resp, err := makeRequest("https://fjeowfjogjweogw.com", client.ln.Addr().String(), t)
	if err == nil {
		t.Errorf("TestIP6 error,err:%v,resp:%v", err, resp)
	}
}

func TestDialTargetErr(t *testing.T) {
	server, client := makeBothStart(newServerDefaultConf(), newClientDefaultConf(), "", t)
	defer server.ln.Close()
	defer client.ln.Close()

	resp, err := makeRequest("https://ipv6.google.com", client.ln.Addr().String(), t)
	if err == nil {
		t.Errorf("TestIP6 error,err:%v,resp:%v", err, resp)
	}
}

func TestWrongServerAddr(t *testing.T) {
	clientConfig := &config.ClientConfig{
		Listen:           0,
		ServerAddr:       "127.0.0.1:1234",
		Url:              "/test",
		ConnNum:          1,
		DnsCleanInterval: 30,
	}
	client := startClient(clientConfig, t)
	defer client.ln.Close()

	resp, err := makeRequest(HTTPS_HOST, client.ln.Addr().String(), t)
	t.Log(err, resp)
	if err == nil {
		t.Errorf("TestWrongServerAddr error,err:%v,resp:%v", err, resp)
	}
}

func TestLsf(t *testing.T) {
	num := 5
	tests := []struct {
		input, expected []int32
	}{
		{[]int32{5, 4, 3, 2, 1}, []int32{1, 2, 3, 4, 5}},
		{[]int32{5, 2, 4, 1, 3}, []int32{1, 2, 3, 4, 5}},
		{[]int32{1, 4, 3, 2, 5}, []int32{1, 2, 3, 4, 5}},
	}
	for _, tt := range tests {
		ch := make(chan *h2Client, num)
		r := make([]int32, num)
		for i := 0; i < num; i++ {
			h2c := &h2Client{i: cid.index(), n: tt.input[i]}
			ch <- h2c
		}
		for i := 0; i < num; i++ {
			e := leastStreamFirst(ch)
			t.Logf("lsf fetch client %d,num:%d", e.i, e.n)
			r[i] = e.n
		}
		if !reflect.DeepEqual(r, tt.expected) {
			t.Errorf("TestLsf fail,expected:%v,result:%v", tt.expected, r)
		}
	}
}

func TestBadHandshake(t *testing.T) {
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		MaxVersion:         tls.VersionTLS12,
	}
	client := http.Client{
		Transport: &http2.Transport{
			TLSClientConfig: tlsConfig,
		},
	}
	sc := newServerDefaultConf()
	server := startServer(sc, nil, AUTH, t)
	defer server.ln.Close()

	url := "https://" + server.ln.Addr().String()
	req, _ := http.NewRequest("GET", url, nil)
	resp, err := client.Do(req)
	if err == nil {
		t.Fatalf("TestBadHandshake error,resp:%v", resp)
	} else {
		t.Log("TestBadHandshake", err, resp)
	}
}

func TestFallback(t *testing.T) {
	client := http.Client{
		Transport: &http2.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	tests := []struct {
		forward string
		isRight bool
	}{
		{"https://theviper.world", true},
		{"http://theviper.world", true},
		{"http://fjweofjewofwe.com", false},
	}
	for _, tt := range tests {
		sc := newServerDefaultConf()
		sc.Forward = tt.forward
		sc.Domain = "localhost"
		server := startServer(sc, nil, AUTH, t)
		_, port := network.GetHostNamePort(server.ln.Addr().String(), false)
		t.Log("port", port)

		urls := []string{"https://localhost:" + port, "https://localhost:" + port + "/dist/bundle-31da9847fd.js"}
		for _, url := range urls {
			req, _ := http.NewRequest("GET", url, nil)
			resp, err := client.Do(req)
			if err != nil {
				t.Errorf("error making request: %v", err)
				if !tt.isRight {
					continue
				} else {
					t.Fatalf("TestFallback error,err:%v", err)
				}
			}
			t.Log(resp.StatusCode, resp.Proto)
			resp.Body.Close()

			if resp.StatusCode != 200 && resp.StatusCode != 404 {
				t.Fatalf("TestFallback error,resp:%v", resp)
			}
		}
		server.ln.Close()
	}
}

func TestWritePwErr(t *testing.T) {
	pr, pw := io.Pipe()
	req, _ := http.NewRequest(http.MethodGet, "https://www.baidu.com", nil)
	errCh := make(chan error, 2)
	wait := make(chan struct{})
	go func() {
		time.Sleep(1 * time.Second)
		writePw(req, errCh, pw)
		t.Log("TestWritePwErr errCh", <-errCh)
		wait <- struct{}{}
	}()
	pw.Close()
	pr.Close()
	<-wait
}

func startServer(serverConfig *config.ServerConfig, tc *timeoutConfig, auth string, t *testing.T) *http2Server {
	server := NewHttp2Server(serverConfig)
	if tc != nil {
		server.tc = tc
	}
	go func() {
		server.Serve([]byte(CERT), []byte(KEY), []byte(auth))
	}()
	<-server.Done
	t.Log("server started")
	return server
}

func startClient(clientConfig *config.ClientConfig, t *testing.T) *http2Client {
	client := NewHttp2Client(clientConfig)
	go func() {
		client.Serve()
	}()
	<-client.done
	t.Log("client started")
	return client
}

func makeBothStart(serverConfig *config.ServerConfig, clientConfig *config.ClientConfig, auth string, t *testing.T) (*http2Server, *http2Client) {
	server := NewHttp2Server(serverConfig)
	client := NewHttp2Client(clientConfig)

	go func() {
		server.Serve(CERT, KEY, []byte(auth))
	}()
	go func() {
		<-server.Done
		t.Log("server started")

		addr := server.ln.Addr().String()
		clientConfig.ServerAddr = addr
		client.Serve()
	}()

	<-client.done
	t.Log("client started")
	return server, client
}

func makeRequest(reqUrl, clientUrl string, t *testing.T) (*http.Response, error) {
	proxyUrl, _ := url.Parse("http://" + clientUrl)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Proxy:           http.ProxyURL(proxyUrl),
	}
	if err := http2.ConfigureTransport(tr); err != nil {
		t.Fatalf("error configuring transport: %v\n", err)
	}
	proxyClient := http.Client{
		Transport: tr,
		Timeout:   3 * time.Second,
	}
	req, _ := http.NewRequest("GET", reqUrl, nil)
	resp, err := proxyClient.Do(req)
	if err != nil {
		t.Logf("error making request: %v", err)
	}
	return resp, err
}
