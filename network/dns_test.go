package network

import (
	"net"
	"testing"
	"time"

	"gitlab.com/RandomCivil/common/log"
)

var host = "www.baidu.com"

func init() {
	log.Level = 1
	log.New()
}

func TestDnsCache(t *testing.T) {
	dnsCache := NewDnsCache(1)
	dnsCache.SetPriority(true)
	go dnsCache.CleanTimer(2)

	var (
		ip  net.IP
		err error
	)
	for i := 0; i < 3; i++ {
		ip, _, err = dnsCache.HandleDnsCache(host)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("domain:%s,ip:%s", host, ip)
	}

	time.Sleep(5 * time.Second)
	dnsCache.SetPriority(false)
	ip, _, err = dnsCache.HandleDnsCache("www.baidu.com")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("domain:%s,ip:%s", host, ip)
}
