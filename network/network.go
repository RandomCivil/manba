package network

import (
	"io"
	"net"
	"net/http"
	"sync"
	"time"

	"gitlab.com/RandomCivil/common/log"
)

func ConnFromHijack(w http.ResponseWriter) net.Conn {
	conn, _, err := w.(http.Hijacker).Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return nil
	}
	return conn
}

const largeBufferSize = 32 * 1024

var lPool = sync.Pool{
	New: func() interface{} {
		return make([]byte, largeBufferSize)
	},
}

func Copy(dst io.Writer, src io.Reader) (int64, error) {
	buf := lPool.Get().([]byte)
	defer lPool.Put(buf)
	return io.CopyBuffer(dst, src, buf)
}

func Proxy(dst io.Writer, src io.Reader, errCh chan error) {
	buf := lPool.Get().([]byte)
	defer lPool.Put(buf)

	n, err := io.CopyBuffer(dst, src, buf)
	log.DefaultLogger.Infof("pipe n:%d,err:%v", n, err)
	errCh <- err
}

func ConnectTarget(dnsCache *DnsCache, domain string) (net.Conn, error) {
	host, port, _ := net.SplitHostPort(domain)
	log.DefaultLogger.Infof("host:%s,domain:%s", host, domain)
	var (
		ip   net.IP
		err  error
		addr string
	)
	if isIp := net.ParseIP(host); isIp == nil {
		ip, _, err = dnsCache.HandleDnsCache(host)
		if err != nil {
			return nil, err
		}
		addr = net.JoinHostPort(ip.String(), port)
	} else {
		addr = domain
	}

	var target net.Conn
	dialer := net.Dialer{Timeout: time.Second * 10}
	target, err = dialer.Dial("tcp", addr)
	if err != nil {
		log.DefaultLogger.Errorf("dial target err:%v", err)
		return nil, err
	}
	return target, nil
}

func GetHostNamePort(host string, isTls bool) (string, string) {
	hostName, port, err := net.SplitHostPort(host)
	if err != nil {
		hostName = host
		if isTls {
			port = "443"
		} else {
			port = "80"
		}
	}
	return hostName, port
}
