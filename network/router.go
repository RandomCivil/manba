package network

import (
	"net"

	"gitlab.com/RandomCivil/common/log"
	router "gitlab.com/RandomCivil/proxy-router"
)

type RouteConfiger interface {
	GetGfwListPath() string
	GetGeoIP2Path() string
	GetRouteStrategy() int
}

type Router struct {
	gfwList *router.GfwList
	Geoip2  *router.GeoIP2
}

func NewRouter(rc RouteConfiger) *Router {
	r := new(Router)
	if rc.GetRouteStrategy() == 1 || rc.GetRouteStrategy() == 2 {
		gfwList := router.NewGfwList(rc.GetGfwListPath())
		gfwList.ParseGfwList()
		r.gfwList = gfwList
	}
	if rc.GetRouteStrategy() == 2 {
		r.Geoip2 = router.NewGeoIP2(rc.GetGeoIP2Path())
	}
	return r
}

func (r *Router) Route(host string, dnsCache *DnsCache, strategy int) (bool, net.IP, error) {
	if r.gfwList.Has(host) {
		return false, nil, nil
	}
	if strategy == 1 {
		return true, nil, nil
	}
	ip, _, err := dnsCache.HandleDnsCache(host)
	if err != nil {
		return false, nil, err
	}
	return r.dealGeoIP2(ip.String()), ip, nil
}

func (r *Router) dealGeoIP2(ip string) bool {
	country := r.Geoip2.ParseGeoip2(ip)
	log.DefaultLogger.Infof("ip %s country :%s", ip, country)
	if country == "CN" {
		return true
	} else {
		return false
	}
}
