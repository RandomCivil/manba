package network

import (
	"os"
	"testing"

	"gitlab.com/RandomCivil/manba/config"
)

var (
	testRootPath string
)

func init() {
	ciEnv := os.Getenv("CI")
	if ciEnv == "true" {
		testRootPath = "/go/src/manba"
	} else {
		testRootPath = os.Getenv("HOME")
	}

}

func TestRoute(t *testing.T) {
	dnsCache := NewDnsCache(180)
	dnsCache.SetPriority(false)
	rc := &config.RouteConfig{
		GfwListPath: testRootPath + "/gfwlist.txt",
		GeoIP2Path:  testRootPath + "/GeoLite2-Country.mmdb",
	}
	tests := []struct {
		host     string
		strategy int
		isDirect bool
	}{
		{"www.shopify.com", 1, true},
		{"www.baidu.com", 1, true},
		{"www.google.com", 1, false},
		{"google.com", 1, false},

		{"www.liulishuo.com", 2, true},
		{"www.shopify.com", 2, false},
		{"www.github.com", 2, false},
		{"www.google.com", 2, false},
		{"google.com", 2, false},
	}

	for _, tt := range tests {
		rc.RouteStrategy = tt.strategy
		r := NewRouter(rc)
		direct, _, _ := r.Route(tt.host, dnsCache, tt.strategy)
		if direct != tt.isDirect {
			t.Fatalf("TestDomainRoute conn:%v,direct expect:%v,get:%v", tt.host, tt.isDirect, direct)
		}
	}
}
