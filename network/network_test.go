package network

import "testing"

type hostNamePort struct {
	hostName, port string
}

func TestGetHostNamePort(t *testing.T) {
	tests := []struct {
		host     string
		isTls    bool
		expected hostNamePort
	}{
		{
			host:     "abc.com:8443",
			expected: hostNamePort{"abc.com", "8443"},
		},
		{
			host:     "abc.com",
			isTls:    true,
			expected: hostNamePort{"abc.com", "443"},
		},
		{
			host:     "abc.com",
			isTls:    false,
			expected: hostNamePort{"abc.com", "80"},
		},
	}
	for _, test := range tests {
		hostName, port := GetHostNamePort(test.host, test.isTls)
		if test.expected.hostName != hostName || test.expected.port != port {
			t.Fatalf("GetHostNamePort err,hostNname:%s,port:%s,expected:%+v\n", hostName, port, test.expected)
		}
	}
}
