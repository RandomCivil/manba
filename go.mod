module gitlab.com/RandomCivil/manba

go 1.19

require (
	github.com/spf13/cobra v1.1.1
	gitlab.com/RandomCivil/common v0.0.0-20210108050723-5ab78e84b182
	gitlab.com/RandomCivil/proxy-router v0.0.0-20210801115201-11e9e007a0fd
	golang.org/x/net v0.0.0-20210428185706-aea814203247
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/oschwald/geoip2-golang v1.4.0 // indirect
	github.com/oschwald/maxminddb-golang v1.6.0 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gitlab.com/RandomCivil/prefix-search v0.0.0-20210801083511-50500e03c888 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
)
