package auth

import (
	"encoding/base64"
	"errors"
	"strings"

	"gitlab.com/RandomCivil/common/log"
	"gopkg.in/yaml.v2"
)

type authInfo struct {
	User string `yaml:"user"`
	Pwd  string `yaml:"pwd"`
}

func LoadAuthList(yamlBytes []byte) (map[string]string, error) {
	if len(yamlBytes) == 0 {
		return nil, nil
	}
	var authInfos []authInfo
	err := yaml.Unmarshal(yamlBytes, &authInfos)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	log.DefaultLogger.Infof("load auth file,%v", authInfos)

	r := make(map[string]string)
	for _, auth := range authInfos {
		r[auth.User] = auth.Pwd
	}
	return r, nil
}

func ParseBasicAuth(authMap map[string]string, auth string) bool {
	if len(auth) == 0 {
		return false
	} else {
		s := strings.SplitN(auth, " ", 2)
		if len(s) != 2 {
			return false
		}
		payload, _ := base64.StdEncoding.DecodeString(s[1])
		pair := strings.SplitN(string(payload), ":", 2)
		if len(pair) != 2 {
			return false
		}

		user, pwd := pair[0], pair[1]
		if v, ok := authMap[user]; ok {
			if v != pwd {
				return false
			} else {
				return true
			}
		} else {
			return false
		}
	}
}
