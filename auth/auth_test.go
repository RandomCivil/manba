package auth_test

import (
	"testing"

	"gitlab.com/RandomCivil/common/cert"
	"gitlab.com/RandomCivil/common/log"
	"gitlab.com/RandomCivil/manba/config"
	"gitlab.com/RandomCivil/manba/core"
)

var (
	KEY, CERT  []byte
	WRONG_YAML = `
- user: admin
  pwd:123456
`
	RIGHT_YAML = `
- user: admin
  pwd: 123456
`
	WRONG_CERT = `-----BEGIN CERTIFICATE-----
MIIDlzCCAn+gAwIBAgIUEI9gylPDUOyst4PtAmhcUPO3/JIwDQYJKoZIhvcNAQEL
BQAwWzELMAkGA1UEBhMCY24xCjAIBgNVBAgMAWExCjAIBgNVBAcMAWIxCjAIBgNV
BAoMAWMxCjAIBgNVBAsMAWQxCjAIBgNVBAMMAWUxEDAOBgkqhkiG9w0BCQEWAWYw
HhcNMTkxMDE0MTMzMzMzWhcNMjAxMDEzMTMzMzMzWjBbMQswCQYDVQQGEwJjbjEK
MAgGA1UECAwBYTEKMAgGA1UEBwwBYjEKMAgGA1UECgwBYzEKMAgGA1UECwwBZDEK
MAgGA1UEAwwBZTEQMA4GCSqGSIb3DQEJARYBZjCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAOo6SC5GAdqu79bel90d3JQgrMIK4fXWZmeEkRXp5Zg6/T3O
O0NF2nO7Zr6BILA8Mu1G9TWIY1sVYFXXbd4f+eJV/9m0+BWG+xp2KKcBMQKFY6hj
Y6T0avvZZvoqyvHHqOZFNVkJ8aa0YklPtuy7LYhhJoydsRgSlfJsu70LN4DLyFLD
Yuf5xRfiBL+YjHjkP8r6DEexgXIlmYDScstJVzML042wqSTk1a7egpw9E8AFaU4W
RZoWNP97Ao5/Qk6bx62BNzEskUxlwh87zfTVyr5wChaQVwg5GnJ5Peoo8aAETd+u
M38X/JdSNzzB+7S4WCtom4eCKk8Uj7d50742tWcCAwEAAaNTMFEwHQYDVR0OBBYE
FAoIn5uQ24rhqwBvA3lGl0cMKTHdMB8GA1UdIwQYMBaAFAoIn5uQ24rhqwBvA3lG
l0cMKTHdMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBALyUMCSv
W8ANcxdvpCTaJYpJPXWtU9/2GsEa+fdRuCImVKHf6dt4qDbrkPvLaOpivsqDNpmo
Zio+dwc/1uhz+OD1/Bbn7a7r/DmYEUR9C0Bv/pvUTaksWUpzCXacPNfhT2kku4CC
gRDUprTBom96fhGk5JIshCWvsCTCoLAhOWTgqbnflSttxnG+AgxTSw3UWTLwby4I
FtzVypPi8phbVe6Mc+7Rm4tFt85jnerv4/I/CUuzWa4Q/vFfpyWCAFlBeLW7GFaj
QTpyzMltpsd43JLUlxAaQl0NKQjNKaWDH+jwMEiqvyijxllqOY3QmqqXovukaIv5
qj5iAyzbqnFct4k=
-----END CERTIFICATE-----
`
	WRONG_KEY = `-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDKIMOAsTJuuyr+
o/uygbt+T14kYBSSUnfdHwWM5CqhzhkXpUWXZqI2evO38eW1VKa2o9/pgEhuE/FL
t6tTZIjjM0pc9I8MJOMmDT/JhB5iRTJLWkhzLdk15YgxSbqRWuveQ7kO9NIiLLOm
A76grUNcU3it7OZ+NO6yxG00YflcFkTyH6Ly38OBXeYoL/AYs0to7bFfyw7HrQKl
0E5Knjmh6/JYGLJ4/RYuFNzBegCbcOFrGodrP2amLAxZVoY82LsaWoCd2s/hD0T1
2x9J2nQsE/wzIYBjlZDJjTLhb56jV9jOLb1jmoXOETO/k9pkDZWhVs044sLLoXT5
rqeavdJZAgMBAAECggEAYOesUzqg7y2Uw1hjB4XjGf97Jv1ZX31iMgT5aD7LoZ9p
sEhwQNHX2z6/MeUrN/BJrS6L3yq9jujIuoBoBycf6a1hbYdiPXRB4rBfcIGC/6Il
JrM2IOJOTOSqktbCN4Xbp0fYpS8Cv5em2WTaM7ZdoRnztY7qQn+mINbDdGW0Y0z/
ywY0YE8QV7EfsYs/zloNMkuetiFDWNL6BoC1eqiGBe8SvKpQalV8oE8by+VGIjTA
9dh1ZWarF4+0MXAEZaJTnbJeGuxpha6lPiwcYbx0nb7kwGNBHq+pLNT+Kb6ZCaIz
r33lU11NA40pkTKyt8JZ/x2m3Xomh50F9cEgN8LncQKBgQDjyS5OmxtvyodGE8wX
dORBfZV4y/ePsaVpdZaok+AKfIiZYylielhAI+p8NvQO37THsYLbZ6OI77fro0le
qWwKjlCLknE3YvGKeb0WscmCofLOJK9G59xgF2JYlEQN9aa/ov930vjH5aAdzeam
vztrRmhekZkzSNwczSjf8gmrMwKBgQDjKgB9MOxgLItH1boJpKxuXG1+mcIECgRy
S61Hf8+uCMO2di5Vh6PE4eK1lBBGeTGQboZGUOGCJbINV8wfBg51sCIbyM8lCg8r
TqRqtabXcvYsI7H+BaQSlADZ/F8iMbC6pE65tT4LNMi2iCVMlppKaZmddfgmIXVr
UIryHhHsQwKBgGuVNDrKBT5acGOHrCgT/E52Kud/pjAG3jId6Ic8OF+bi8SuUfiC
AjZhADmklsaDn9WFIGHR5Coj1kEDwP5llHKz3pTdGH3pTF1zu/wBAkShipUUPHiH
v8zenoNpm27ga94s9O26BegbqvEIs3xj64NyII8xwyK2scckmr6SiUCDAoGAa4rM
5Heuz/EGlyF1i28sOqsDIzZDhYoAhOOSyxCVDz0S+mSElvU58NFHdNL9yX9Cma/Z
XZyYxfZ2jp6MAfvqCIkz/JdaiZxzhfsbF3Gb5M+F/2t+rlWZUTpEFO7HUvbXReTX
aE+HaeK5SsC1d8askKHhmYvpyJN6dS2SOgFGVuUCgYEAxJZE9cZgwiFtZwchXVcU
x6pa6qbSxhbRNV9SsGnrGK1fw1zkPwbQ8wjwO4XiQYKIKEmjkFwKnNYH2Pr6YOG5
35eILyPg7l1Sw0CRjST5N3PeFI6EqXC2oFYbgSufXI7QoFv0MyOH6eHgTBuUnc4r
An9a3kGNEZFuhiFJD7euL9Q=
-----END PRIVATE KEY-----
`
)

func init() {
	log.Level = 1
	log.New()

	CERT, KEY = cert.GenCert()
}

func TestWrongYaml(t *testing.T) {
	serverConfig := &config.ServerConfig{
		Listen:           8442,
		DnsCleanInterval: 60,
	}
	server := core.NewHttp2Server(serverConfig)
	err := server.Serve(CERT, KEY, []byte(WRONG_YAML))
	if err == nil {
		t.Fatal("TestWrongYaml error")
	}
}

func TestWrongCert(t *testing.T) {
	serverConfig := &config.ServerConfig{
		Listen:           8442,
		DnsCleanInterval: 60,
	}
	server := core.NewHttp2Server(serverConfig)
	err := server.Serve([]byte(WRONG_CERT), []byte(WRONG_KEY), []byte(RIGHT_YAML))
	if err == nil {
		t.Fatal("TestWrongCert error")
	}
}
