ARG BUILD_IMAGE_URL
FROM $BUILD_IMAGE_URL as build

RUN make build BIN_DIR=/go/bin

FROM alpine:3.18
COPY --from=build /go/bin/manba /root
ENTRYPOINT ["/root/manba"]
