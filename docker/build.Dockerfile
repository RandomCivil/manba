FROM theviper/proxy-resource:latest as geolite
FROM golang:1.19-alpine

WORKDIR /go/src/manba

RUN apk add --no-cache gcc musl-dev bash curl make git

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

COPY --from=geolite /root/gfwlist.txt /go/src/manba/gfwlist.txt
COPY --from=geolite /root/GeoLite2-Country.mmdb /go/src/manba/GeoLite2-Country.mmdb

# RUN rm -r ~/.cache/go-build/
