# manba

proxy via [HTTP2](https://en.wikipedia.org/wiki/HTTP/2)

[![pipeline status](https://gitlab.com/RandomCivil/manba/badges/master/pipeline.svg)](https://gitlab.com/RandomCivil/manba/commits/master) [![codecov](https://codecov.io/gl/RandomCivil/manba/branch/master/graph/badge.svg?token=pPGJaxHjzg)](https://codecov.io/gl/RandomCivil/manba)

[DOC](https://randomcivil.gitlab.io/doc/manba/)
